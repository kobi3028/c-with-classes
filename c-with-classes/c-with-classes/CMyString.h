#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef struct _CMyString
{
	char* m_str;
	size_t m_length;
	size_t m_size;
}CMyString;