#include "CMyString.h"

void Clear(CMyString* self);
void Assign_str(CMyString* self, char* otherString, size_t length);

int Init_CMyString(CMyString* self)
{
	self->m_length = 0;
	self->m_size = 0;
	self->m_str = NULL;
	return 0;
}

int Init_CMyString_str(CMyString* self, char* otherString, size_t length)
{
	self->m_length = 0;
	self->m_size = 0;
	self->m_str = NULL;
	Assign_str(self, otherString, length);
	return 0;
}

int Dtor_CMyString(CMyString* self)
{
	Clear(self);
	return 0;
}

size_t GetLength(CMyString* self)
{
	return self->m_length;
}

char* getStr(CMyString* self)
{
	return self->m_str;
}

void Assign(CMyString* self, CMyString* otherString)
{
	size_t len = otherString->m_length;
	if(len >= self->m_size)
	{
		Clear(self);
		self->m_str = (char*)malloc((len + 1) * sizeof(char));
		self->m_size = len + 1;
	}
	memcpy_s(self->m_str, len, otherString->m_str, len);
	self->m_str[ len ] = '\0';
	self->m_length = len;
}

void Assign_str(CMyString* self, char* otherString, size_t length)
{
	size_t len = length <= strlen(otherString) ? length : strlen(otherString);
	if(len >= self->m_size)
	{
		Clear(self);
		self->m_str = (char*)malloc((len + 1) * sizeof(char));
		self->m_size = len + 1;
	}
	memcpy_s(self->m_str, len, otherString, len);
	self->m_str[ len ] = '\0';
	self->m_length = len;
}

char CharAt(CMyString* self, size_t index)
{
	if(index < self->m_length)
	{
		return self->m_str[ index ];
	}
	printf("unvalid index");
	return '\0';
}

void Append(CMyString* self, CMyString* otherString)
{
	size_t len = self->m_length + otherString->m_length;
	if(self->m_length + otherString->m_length >= self->m_size)
	{
		size_t length = self->m_length;
		char* tmp = (char*)malloc((length + 1) * sizeof(char));
		memcpy(tmp, self->m_str, length);
		tmp[ length ] = '\0';
		Clear(self);
		self->m_str = (char*)malloc((len + 1) * sizeof(char));
		self->m_size = len + 1;
		memcpy_s(self->m_str, length, tmp, length);
		self->m_str[ length ] = '\0';
	}
	strncat_s(self->m_str, self->m_size, otherString->m_str, otherString->m_length);
	self->m_str[ len ] = '\0';
	self->m_length = len;
}

int Compare(CMyString* self, CMyString* otherString)
{
	return strcmp(self->m_str, otherString->m_str);
}

int IsEmpty(CMyString* self)
{
	return self->m_length == 0;
}

void Clear(CMyString* self)
{
	if(self->m_str)
	{
		free(self->m_str);
		self->m_str = NULL;
	}
	self->m_length = 0;
	self->m_size = 0;
}

int main(void)
{
	char* str1 = "this is my first string";
	char* str2 = "this is my second string";
	CMyString s1;
	CMyString s2;
	Init_CMyString_str(&s1, str1, strlen(str1));
	Init_CMyString_str(&s2, str2, strlen(str2));

	printf("s1 is empty: %d\n",IsEmpty(&s1));
	printf("s2 is empty: %d\n", IsEmpty(&s2));
	printf("s1 Length: %d\n", GetLength(&s1));
	printf("s2 Length: %d\n", GetLength(&s2));
	printf("s1 longer then s2: %d\n", Compare(&s1, &s2));
	printf("s1[10]: %c\n", CharAt(&s1, 10));
	printf("s2[10]: %c\n", CharAt(&s2, 10));

	Append(&s1, &s2);
	printf("s1 str: %s\r\ns2 str: %s\r\n", getStr(&s1), getStr(&s2));
	Assign(&s1, &s2);
	printf("s1 str: %s\r\ns2 str: %s\r\n", getStr(&s1), getStr(&s2));
	return 0;
}